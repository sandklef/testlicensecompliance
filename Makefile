JAVA_SRC=com/sandklef/test/HelloCleveland.java
JAVA_CLASSES=$(JAVA_SRC:%.java=%.class)

all: $(JAVA_CLASSES)

%.class:%.java
	javac $<

test: $(JAVA_CLASSES)
	java com.sandklef.test.HelloCleveland
